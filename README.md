# Clion-Mac-stm32

#### 介绍
Clion在Mac（基于M1 pro芯片）系统下配置stm32开发环境。

#### 一、安装Clion
可自行去[官网下载](https://www.jetbrains.com/clion/)

#### 二、安装homebrew
1.  homebrew缺失的软件包的管理器
2.  打开下载[官网](https://brew.sh/index_zh-cn)
3.  复制命令行
![输入图片说明](picture/homebrew.jpg)
4.  打开终端粘贴回车运行

#### 三、安装Java环境
STM32Cubemx需要Java运行环境，点击进行[下载](https://www.java.com/zh-CN/)。
下载完成后双击进行安装。

#### 四、安装STM32Cubemx
[官网下载地址](https://www.st.com/en/development-tools/stm32cubemx.html#get-software)，
该软件基于x86架构，在Apple silicon上运行需要rosetta2，若你的电脑没有安装，可以在终端输入

softwareupdate --install-rosetta命令完成安装。

#### 五、安装ARM-GCC工具链
1. 在安装此工具链的时候，建议安装相应的加速器
2. 打开终端输入

brew tap ArmMbed/homebrew-formulae

brew install arm-none-eabi-gcc

![输入图片说明](picture/GCC.jpg)
完成安装后输入arm-none-eabi-gcc -v查看版本信息。
![输入图片说明](picture/GCC1.jpg)
如图所示即为安装成功。

#### 六、OpenoCD安装
[下载](https://github.com/xpack-dev-tools/openocd-xpack/releases/tag/v0.11.0-4)
![输入图片说明](picture/opened.jpg)
下载完成后双击解压缩，然后将此文件夹保存（记住保存路径）。

#### 七、测试
1.  打开Clion，点击右上角Clion-偏好设置-嵌入式开发，添加之前opencd路径，点击测试，出现此提示即为安装成功。
![输入图片说明](picture/text.jpg)
2.安装插件Embedded Development Support
![输入图片说明](picture/other.jpg)
3.重启Clion，新建STM32Cubemx工程
![输入图片说明](picture/new.jpg)![输入图片说明](picture/1.jpg)![输入图片说明](picture/2.jpg)


大功告成


#### 特此感谢

1.  参考[配置CLion用于STM32开发【优雅の嵌入式开发】](https://zhuanlan.zhihu.com/p/145801160)
2.  参考[Mac搭建STM32环境（基于Apple silicon）](https://zhuanlan.zhihu.com/p/503001037)

